import React from 'react';
import { Stylesheet, Text, View } from 'react-native';
import { Camera, Permissions } from 'expo';
import { Container, Content, Header, Item, Icon, Input, Button } from 'native-base';
import MaterialCommunityIcons from '@expo/vector-icons/MaterialCommunityIcons';

class CameraComponent extends React.Component {
    state = {
        hasCameraPermission: null,
        type: Camera.Constants.Type.back
    }

    async componentWillMount() {
        const {status} = await Permissions.askAsync(Permissions.Camera);
        this.setState({hasCameraPermission: status === 'granted'})
    }

    render() {
        const {hasCameraPermission} = this.state
        if (hasCameraPermission === null) {
            return <View />
        } else if (hasCameraPermission === false) {
            return <Text>No access to Camera</Text>
        } else {
            return (
                <View style={{ flex: 1 }}>
                    <Camera style={{ flex: 1 }} type={this.state.type} />
                        
                    
                </View>
            )
        }
    }
}
export default CameraComponent;

